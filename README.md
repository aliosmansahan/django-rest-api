**REQUIREMENTS**

- Python 3.9.5
- Django 3.2.4
- PostgreSQL 13.3
- Redis-cli 6.2.4
- Insomnia RestAPI Client 2021.3.0

**Installing Necessery Django Packages**

- `pip install djangorestframework # v3.12.4`
- `pip install markdown            # v3.3.4`
- `pip install django-filter       # v2.4.0`
- `pip install django-softdelete   # v0.9.2`
- `pip install django-redis        # v5.0.0` 
- `pip install pylibmc             # v1.6.1`

**Installation**

- Create your PostgreSQL database and set database variables in `settings.py`. 
- Run `python3 manage.py migrate` command for create database tables.
- Use `python3 manage.py createsuperuser` command for create your admin user. This user necessery for login to admin panel and rest api authentication.

**Run Project**

- Use `python3 manage.py runserver` comamnd for server run. 
- Run `redis-cli monitor` command to inspect the caching variables.   

Project runnig on `http://localhost:8000/`. You can use `http://localhost:8000/vehicles/` link for vehicles and `http://localhost:8000/vehicle_models/` for vehicle models. However you can use `Insomnia API Client` with `Insomnia_django_rest_api.json` file, for rest api requests.  You can access to admin panel with `http://localhost:8000/admin` link and can manage users, vehicles, vehicle models.
