"""rentacar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
# from django.urls import path

# urlpatterns = [
#     path('admin/', admin.site.urls),
# ]


# from django.conf.urls import url
# from django.contrib import admin
# from rest_framework.urlpatterns import format_suffix_patterns
# from API import views
# from django.urls import path, include
# from rest_framework import routers, viewsets, serializers


# router = routers.DefaultRouter()
# # router.register(r'vehicles', views.VehicleViewSet.as_view({'get': 'list'}), 'vehicles')
# # router.register(r'terms', views.TermViewSet, 'terms')
# # router.register(r'disciplines', views.DisciplineViewSet, 'disciplines')


# urlpatterns = [
#     path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
#     url(r'^admin/', admin.site.urls),

#     path('vehicles/', include((router.urls, 'app_name'))),

#     # url(r'^vehicles/', views.VehicleViewSet.as_view()),
#     # url(r'^vehicle_models/', views.VehicleModelList.as_view()),

# ]

from django.conf.urls import url,include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from API import views
from django.urls import path, include
from rest_framework import routers, viewsets, serializers
from rest_framework import routers




router = routers.DefaultRouter()
router.register(r'vehicles', views.VehicleViewSet)
router.register(r'vehicle_models', views.VehicleModelViewSet)
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
     url(r'^', include(router.urls))
]


