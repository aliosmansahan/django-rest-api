from rest_framework import serializers
from .models import Vehicle, VehicleModel
from django.contrib.auth.models import User

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']
        # fields = '__all__' #For all fields

class VehicleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vehicle 
        fields = ['km', 'plate', 'chassis_number', 'color', 'vehicle_model_id_id']
        # fields = '__all__' #For all fields

class VehicleModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = VehicleModel 
        fields = ['name', 'brand']
        # fields = '__all__' #For all fields
