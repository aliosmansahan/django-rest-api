from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, serializers, viewsets
from django.contrib.auth.models import User
from .models import Vehicle, VehicleModel
from .serializers import VehicleSerializer, VehicleModelSerializer, UserSerializer
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60 * 1))
    def dispatch(self, *args, **kwargs):
        return super(UserViewSet, self).dispatch(*args, **kwargs)

    # Another method for caching
    # def retrieve(self, request, pk=None):
    #     cache_key = f"user_details_{pk}"

    #     data = cache.get(cache_key)
    #     if data:
    #         return Response(data)

    #     response = super().retrieve(request, pk=pk)
    #     cache.set(cache_key, response.data)
    #     return response

class VehicleViewSet(viewsets.ModelViewSet):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer

    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60 * 1))
    def dispatch(self, *args, **kwargs):
        return super(VehicleViewSet, self).dispatch(*args, **kwargs)


class VehicleModelViewSet(viewsets.ModelViewSet):
    queryset = VehicleModel.objects.all()
    serializer_class = VehicleModelSerializer

    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60 * 1))
    def dispatch(self, *args, **kwargs):
        return super(VehicleModelSerializer, self).dispatch(*args, **kwargs)



