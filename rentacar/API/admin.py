from django.contrib import admin

from django.contrib import admin
from .models import Vehicle, VehicleModel

admin.site.register(Vehicle)
admin.site.register(VehicleModel)