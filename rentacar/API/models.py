from django.db import models
from softdelete.models import SoftDeleteObject

class BaseModel(SoftDeleteObject, models.Model):
    created_at    = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

#You can use this command block for need to SoftDeletionManager.
# class SoftDeletionManager(models.Manager):
#     def __init__(self, *args, **kwargs):
#         self.alive_only = kwargs.pop('alive_only', True)
#         super(SoftDeletionManager, self).__init__(*args, **kwargs)

#     def get_queryset(self):
#         if self.alive_only:
#             return SoftDeletionQuerySet(self.model).filter(deleted_at=None)
#         return SoftDeletionQuerySet(self.model)

#     def hard_delete(self):
#         return self.get_queryset().hard_delete()

# class SoftDeletionModel(BaseModel):
#     deleted_at = models.DateTimeField(blank=True, null=True)

#     objects = SoftDeletionManager()
#     all_objects = SoftDeletionManager(alive_only=False)

#     class Meta:
#         abstract = True

#     def delete(self):
#         self.deleted_at = timezone.now()
#         self.save()

#     def hard_delete(self):
#         super(SoftDeletionModel, self).delete()

class VehicleModel(BaseModel):
    name = models.CharField(max_length=200)
    brand = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Vehicle(BaseModel):
    km = models.IntegerField(default=0)
    plate = models.CharField(max_length=20)
    chassis_number = models.CharField(max_length=17)
    color  = models.CharField(max_length=20)
    vehicle_model_id = models.ForeignKey(VehicleModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.plate




