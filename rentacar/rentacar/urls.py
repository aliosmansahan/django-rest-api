from django.conf.urls import url,include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from API import views
from django.urls import path, include
from rest_framework import routers, viewsets, serializers

router = routers.DefaultRouter()
router.register(r'vehicles', views.VehicleViewSet)
router.register(r'vehicle_models', views.VehicleModelViewSet)
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
     url(r'^', include(router.urls))
]


